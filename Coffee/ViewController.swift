//
//  ViewController.swift
//  Coffee
//
//  Created by Timur Astashov on 7/24/18.
//  Copyright © 2018 Timur Astashov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view.backgroundColor = .black
    }
    let coffeeMachineView = CoffeeMachine()
    
    @IBAction func DoingLatteButton() {
        coffeeMachineView.doingLatte()
        Monitor.text = coffeeMachineView.backCoffeeMissions()
    }
    
    @IBOutlet weak var Monitor: UILabel!
    
    @IBAction func AddWaterButton() {
        Monitor.text = coffeeMachineView.addWater()
    }
    
    @IBAction func AddCoffeeButton() {
        Monitor.text = coffeeMachineView.addCoffee()
    }
    
    @IBAction func AddMilkButton() {
        Monitor.text = coffeeMachineView.addMilk()
    }
    
    @IBAction func DoingAmericanoWithoutMilkButton() {
        coffeeMachineView.doingAmericanoWithoutMilk()
        Monitor.text = coffeeMachineView.backCoffeeMissions()
    }
    
    @IBAction func DoingAmericanoWithMilkButton() {
        coffeeMachineView.doingAmericanoWithMilk()
        Monitor.text = coffeeMachineView.backCoffeeMissions()
    }
    
    @IBAction func DoingFlatWhiteButton() {
        coffeeMachineView.doingFlatWhite()
        Monitor.text = coffeeMachineView.backCoffeeMissions()
    }
    
    @IBAction func DoingRistrettoButton() {
        coffeeMachineView.doingRistretto()
        Monitor.text = coffeeMachineView.backCoffeeMissions()
    }
    
    @IBAction func DoingCortadoButton() {
        coffeeMachineView.doingCortado()
        Monitor.text = coffeeMachineView.backCoffeeMissions()
    }
    
    
    @IBAction func CleaningButton() {
        Monitor.text = coffeeMachineView.cleanTrash()
    }
}

