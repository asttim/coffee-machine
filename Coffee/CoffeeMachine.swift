//
//  CoffeeMachine.swift
//  Coffee
//
//  Created by Timur Astashov on 7/24/18.
//  Copyright © 2018 Timur Astashov. All rights reserved.
//

import UIKit

class CoffeeMachine: NSObject {
    var water = 0.0
    var coffee = 0.0
    var milk = 0.0
    var trash = 0.0
    var problem = ""
    let successText = "Here is your coffee"
    func testOnAvailabilityAndUsing(neededWater: Double, neededCoffee: Double, needeedMilk: Double) {
        problem = ""
        if water < neededWater {
            problem += "Add water"
        }
        if coffee < neededCoffee {
            problem += " Add coffee"
        }
        if milk < needeedMilk {
            problem += " Add milk"
        }
        if trash > 77{
            problem += " Clean trash"
        }
        print(backCoffeeMissions())
        if problem == "" {
            water -= neededWater
            coffee -= neededCoffee
            milk -= needeedMilk
            trash += neededCoffee
        }
    }
    
    func doingLatte() {
        testOnAvailabilityAndUsing(neededWater: 400, neededCoffee: 23, needeedMilk: 30)
    }
    
    func doingAmericanoWithMilk() {
        testOnAvailabilityAndUsing(neededWater: 320, neededCoffee: 25, needeedMilk: 30)
    }
    
    func doingAmericanoWithoutMilk() {
        testOnAvailabilityAndUsing(neededWater: 400, neededCoffee: 25, needeedMilk: 0)
    }
    
    func doingFlatWhite() {
        testOnAvailabilityAndUsing(neededWater: 300, neededCoffee: 140, needeedMilk: 10)
    }
    
    func doingRistretto() {
        testOnAvailabilityAndUsing(neededWater: 150, neededCoffee: 70, needeedMilk: 0)
    }
    
    func doingCortado() {
        testOnAvailabilityAndUsing(neededWater: 100, neededCoffee: 70, needeedMilk: 100)
    }
    
    func addWater() -> String {
        water += 770
        let waterBalance = "You have \(water) mililiters  of water"
        print(waterBalance)
        return waterBalance
    }
    
    func addMilk() -> String {
        milk += 120
        let milkBalance = "You have \(milk) mililiters of milk"
        print(milkBalance)
        return milkBalance
    }
    
    func addCoffee() -> String {
        coffee += 70
        let coffeeBalance = "You have \(coffee) coffee beans"
        print(coffeeBalance)
        return coffeeBalance
    }
    
    func cleanTrash() -> String {
        trash = 0.0
        let trashIsCleaned = "Trash Is Cleaned!!!"
        return trashIsCleaned
    }
    
    func backCoffeeMissions() -> String {
        if problem == "" {
            return successText
        } else {
            return problem
        }
    }
    
}


